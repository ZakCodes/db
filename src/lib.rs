#![feature(test)]
extern crate test;
extern crate bson;
extern crate byteorder;
extern crate serde;

mod file;
mod db;
mod db_infos;

pub use db::*;

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::copy;
    use std::fs::File;
    use test::Bencher;

    #[test]
    fn open() {
        let a = "a.db";
        let b = "b.db";

        // create the db
        DB::<User>::create(a, 10, 1024 * 1024, &["name", "email", "age", "weight"]).unwrap();

        // copy the db to another db
        copy(&mut File::open(a).unwrap(), &mut File::create(b).unwrap()).unwrap();

        let a: DB<User> = DB::new(a, 0, 0, &[]).unwrap();
        let b: DB<User> = DB::load(b).unwrap();

        assert!(a.eq(&b));
    }

    #[bench]
    fn bench_open(b: &mut Bencher) {
        b.iter(|| {
            DB::<User>::create("bench.db", 10000, 1024 * 1024, &["name", "email", "age", "weight"]).unwrap();
        });
    }

    #[derive(PartialEq, Debug)]
    struct User {
        id: u32,
        name: String,
        email: String,
        age: u8,
        weight: f32
    }
}