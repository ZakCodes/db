pub use std::fs::File;
pub use std::io::{Read, Write, Result, Error, ErrorKind};
use std::io::{Seek, SeekFrom};
pub use byteorder::{ReadBytesExt, WriteBytesExt, BigEndian};

pub trait DBFile: Seek+Read+Write+ReadBytesExt+WriteBytesExt {
    fn seek_start(&mut self, pos: u64) -> Result<u64> {
        self.seek(SeekFrom::Start(pos))
    }
    fn seek_current(&mut self, pos: i64) -> Result<u64> {
        self.seek(SeekFrom::Current(pos))
    }
    fn pos(&mut self) -> Result<u64> {
        self.seek_current(0)
    }

    fn read_str(&mut self) -> Result<String> {
        let size: u32 = self.read_u32_be()?;
        Ok(unsafe {
            let mut buf = vec![0u8; size as usize];
            self.read(&mut buf)?;
            String::from_utf8_unchecked(buf)
        })
    }

    fn write_str(&mut self, string: &str) -> Result<usize> {
        self.write(string.as_bytes())
    }

    #[inline]
    fn read_u32_be(&mut self) -> Result<u32> {
        self.read_u32::<BigEndian>()
    }
    #[inline]
    fn read_u64_be(&mut self) -> Result<u64> {
        self.read_u64::<BigEndian>()
    }
    #[inline]
    fn write_u32_be(&mut self, x: u32) -> Result<()> {
        self.write_u32::<BigEndian>(x)
    }
    #[inline]
    fn write_u64_be(&mut self, x: u64) -> Result<()> {
        self.write_u64::<BigEndian>(x)
    }
}

impl DBFile for File {

}