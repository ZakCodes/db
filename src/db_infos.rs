use file::*;
use std::mem::size_of;

#[derive(Debug, PartialEq, Eq)]
pub struct DBInfos {
    size: u32, // Number of stored objects
    capacity: u32, // Number of objects that can be stored
    next_id: u32, // Id that can be used for a new object
    last_object: u64, // Pointer after the last object of the DB
}

macro_rules! db_field {
($name:ident, $write_name:ident, $offset:expr, u32) => {
    db_field!($name, $write_name, $offset, u32, write_u32_be);
};
($name:ident, $update_name:ident, $offset:expr, $ty:ty, $write_ty:ident) => {
    pub fn $name(&self) -> $ty {
        self.$name
    }

    pub fn $update_name(&mut self, $name: $ty, file: &mut File) -> Result<()> {
        file.seek_start($offset as u64)?;
        file.$write_ty($name)?;

        self.$name = $name;
        Ok(())
    }
}
}

impl DBInfos {
    db_field!(size, update_size, 0, u32);
    db_field!(capacity, update_capacity, size_of::<u32>(), u32);
    db_field!(next_id, update_next_id, size_of::<u32>() * 2, u32);
    db_field!(last_object, update_last_object, size_of::<u32>() * 3, u64, write_u64_be);

    pub fn create(size: u32, capacity: u32, next_id: u32, last_object: u64, file: &mut File) -> Result<Self> {
        file.write_u32_be(size)?;
        file.write_u32_be(capacity)?;
        file.write_u32_be(next_id)?;
        file.write_u64_be(last_object)?;

        Ok(DBInfos {
            size,
            capacity,
            next_id,
            last_object
        })
    }
    pub fn load(file: &mut File) -> Result<Self> {
        Ok(DBInfos {
            size: file.read_u32_be()?,
            capacity: file.read_u32_be()?,
            next_id: file.read_u32_be()?,
            last_object: file.read_u64_be()?
        })
    }
}