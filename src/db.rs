use std::marker::PhantomData;
use std::collections::HashMap;
use std::path::Path;
use std::io::{Error, ErrorKind};
use std::mem::size_of;
use file::*;
use db_infos::DBInfos;

pub struct DB<T> {
    file: File,
    infos: DBInfos,
    registered_properties: HashMap<String, (u64, u64)>, // prop_name, (register, order)

    phantom: PhantomData<T>
}

impl<T> DB<T> {
    /// Tries to load the database and creates it if it doesn't exist.
    /// Still returns an error if the file exists but the database couldn't be read from it.
    pub fn new(path: &str, capacity: u32, file_size: u64, registered_properties: &[&str]) -> Result<Self> {
        match Self::load(path) {
            Ok(db) => Ok(db),
            Err(e) => {
                if e.kind() == ErrorKind::NotFound {
                    Self::create(path, capacity, file_size, registered_properties)
                }
                else {
                    Err(e)
                }
            }
        }
    }
    pub fn create(path: &str, capacity: u32, file_size: u64, registered_properties: &[&str]) -> Result<Self> {
        // Create and size the file
        let mut file = File::create(path)?;
        file.set_len(file_size)?;

        // Generate the database informations and write them to the file
        let mut infos = DBInfos::create(0, capacity, 0, 0, &mut file)?;

        // Get and write the number of registered properties
        let registered_properties_count = registered_properties.len();
        file.write_u32_be(registered_properties_count as u32)?;

        // Get the position after the properties (without writing them to the file)
        let mut after_reg_props: u64 = {
            let registered_properties_size = registered_properties.iter().fold(0usize, |acc, s| acc + Property::<T>::compute_size(*s));
            file.pos()? + (registered_properties_size as u64)
        };

        // Get the size of the order of a property
        let prop_order_size: u64 = (3 * size_of::<u64>() as u64) * capacity as u64;

        // Write each registered properties
        let mut registered_properties_map: HashMap<String, (u64, u64)> = HashMap::with_capacity(registered_properties_count);
        for prop in registered_properties {
            file.write_str(*prop)?;
            registered_properties_map.insert(String::from(*prop), (file.pos()?, after_reg_props));
            file.write_u64_be(after_reg_props)?;

            after_reg_props += prop_order_size;
        }

        // Write the last_object ptr
        infos.update_last_object(after_reg_props, &mut file)?;

        Ok(DB {
            file,
            infos,
            registered_properties: registered_properties_map,

            phantom: PhantomData
        })
    }
    pub fn load(path: &str) -> Result<Self> {
        // If the file exists
        if Path::new(path).exists() {
            // Load the database
            let mut file = File::open(path)?;

            // Get the database informations
            let infos = DBInfos::load(&mut file)?;

            // Get the registered properties
            let registered_properties_count = file.read_u8()?;
            let mut registered_properties: HashMap<String, (u64, u64)> = HashMap::with_capacity(registered_properties_count as usize);
            for _ in 0..registered_properties_count {
                registered_properties.insert(file.read_str()?, (file.pos()?, file.read_u64_be()?));
            }

            Ok(DB {
                file,
                infos,
                registered_properties,

                phantom: PhantomData
            })
        }
        else {
            // Return an error because the database doesn't exist
            Err(Error::new(ErrorKind::NotFound, "The database file wasn't found"))
        }
    }

    pub fn insert(&mut self, t: &mut T) -> Result<()> {
        unimplemented!()
    }
}

impl<T> PartialEq for DB<T> {
    fn eq(&self, rhs: &Self) -> bool {
        self.infos == rhs.infos
    }
}
